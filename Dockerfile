FROM node:20-alpine as build
WORKDIR ./app

COPY ./ ./

RUN npm install
RUN npm run build --prod

FROM nginx:alpine

COPY ./default.conf /etc/nginx/conf.d
COPY --from=build /app/dist/heroes/browser /usr/share/nginx/html
