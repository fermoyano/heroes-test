import {BehaviorSubject, combineLatest, map, Observable, ReplaySubject, Subject, switchMap, takeUntil} from "rxjs";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {inject} from "@angular/core";

export interface Page {
  pageSize: number;
  pageIndex: number;
}

export abstract class PaginatedRequest<T> {

  private _pageParams$ = new ReplaySubject<Page>();
  private _itemCount$ = new ReplaySubject<number>();
  private _update$ = new BehaviorSubject(true);
  destroy$ = new Subject()

  abstract url: string;
  params = {};

  defaultPage = {
    pageIndex: 0,
    pageSize: 10,
  }

  protected http = inject(HttpClient);
  private router = inject(Router);
  private route = inject(ActivatedRoute);

  constructor() {
    this.getParamsFromUrl();
  }

  getAll(): Observable<T[]> {
    return combineLatest([
      this.paginationHeaders$,
      this._update$
    ]).pipe(
      switchMap(([pagination, _]: [HttpHeaders, boolean]) => this.http.get<T[]>(this.url, {
        headers: pagination,
        observe: "response",
        params: this.params,
      })),
      map((res) => this.extractCount(res)),
    )
  }

  setParams(params: any): void {
    this.params = params;
  }

  getParamsFromUrl() {
    this.route.queryParams.pipe(
      takeUntil(this.destroy$)
    ).subscribe(({pageIndex, pageSize}) => {
      if (!!pageIndex && !!pageSize) {
        this.setPage({pageIndex: parseInt(pageIndex), pageSize: parseInt(pageSize)});
      } else {
        this.setPage(this.defaultPage);
      }
    });
  }

  setPage(page: Page) {
    this._pageParams$.next(page);
    this.router.navigate([], {
      queryParams: page,
      replaceUrl: true
    });
  }

  updateData() {
    this._update$.next(true);
  }

  get pageParams$(): Observable<Page> {
    return this._pageParams$
  }

  extractCount(response: HttpResponse<T[]>): T[] {
    const count: number = parseInt(response.headers.get('X-Count') || '0');
    this._itemCount$.next(count);

    return response.body || [];
  }

  get paginationHeaders$(): Observable<HttpHeaders> {
    return this._pageParams$.pipe(
      map(page => new HttpHeaders({
        'page-index': page.pageIndex.toString(),
        'page-size': page.pageSize.toString(),
      }))
    )
  }

  get itemCount$(): Observable<number> {
    return this._itemCount$
  }
}
