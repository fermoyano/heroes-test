export interface Hero {
  id: number;
  name: string;
  realName: Name,
}

export interface Name {
  firstName: string;
  middleName: string;
  lastName: string;
}
