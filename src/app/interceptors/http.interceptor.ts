import {HttpInterceptorFn} from '@angular/common/http';
import {inject} from "@angular/core";
import {delay, finalize} from "rxjs";
import {LoadingRequestService} from "../services/loading-request/loading-request.service";

export const httpInterceptor: HttpInterceptorFn = (req, next) => {
  const loadingService = inject(LoadingRequestService);
  loadingService.setLoading(true);
  let headers = req.headers.append('Token', 'fake-token');
  headers = headers.append('Access-Control-Allow-Origin', '*');

  const newReq = req.clone({
    headers
  });

  return next(newReq).pipe(
    delay(1000),
    finalize(() => loadingService.setLoading(false))
  );
};
