import {HttpHeaders, HttpInterceptorFn, HttpResponse} from '@angular/common/http';
import {MockServerData} from "./mock-server";
import {of} from "rxjs";

const mockServer = new MockServerData();

export const mockBackendInterceptor: HttpInterceptorFn = (req, next) => {

  const idParam = parseInt(req.url.match('heroes/([1-9]*)')?.at(1) || '');
  const nameParameter = req.params.get('name');


  if (req.method === 'GET' && nameParameter) {
    const body = mockServer.search(nameParameter)
    const res = new HttpResponse({status: 200, body})
    return of(res);
  }

  if (req.method === 'GET' && !idParam) {
    const body = mockServer.get(req)
    const res = new HttpResponse({status: 200, body, headers: new HttpHeaders({"X-count": body.length})})
    return of(res);
  }

  if (req.method === 'GET' && !!idParam) {
    const body = mockServer.getById(idParam);
    const res = new HttpResponse({status: 200, body})
    return of(res);
  }

  if (req.method === 'PUT') {
    const body = mockServer.update(idParam, req)
    const res = new HttpResponse({status: 200, body})
    return of(res);
  }

  if (req.method === 'POST') {
    const body = mockServer.new(req)
    const res = new HttpResponse({status: 201, body})
    return of(res);
  }

  if (req.method === 'DELETE') {
    mockServer.delete(idParam)
    const res = new HttpResponse({status: 200})
    return of(res);
  }

  return next(req);
};
