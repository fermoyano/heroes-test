import {factoryMockData, idGenerator} from "./hero-mock-data";
import {Hero} from "../../models/hero";
import {HttpRequest} from "@angular/common/http";

export class MockServerData {

  heroes: Hero[] = factoryMockData(1);

  get(request: HttpRequest<any>): Hero[] {
    const nameFilter = request.params.get('name');

    if (nameFilter) {
      return this.heroes.filter(hero => hero.name.toLowerCase().includes(nameFilter.toLowerCase()));
    }

    return this.heroes
  }

  getById(id: number): Hero {
    return <Hero>this.heroes.find(hero => hero.id == id);
  }

  update(id: number, request: HttpRequest<any>): Hero {
    const index = this.heroes.findIndex(hero => hero.id == id);

    this.heroes[index] = request.body;
    return this.heroes[index];
  }

  new(req: HttpRequest<any>) {
    const newHero: Hero = {
      id: idGenerator(),
      name: req.body.name,
      realName: req.body.realName,
    }

    this.heroes.push(newHero);
    return newHero;
  }

  search(name: string) {
    return this.heroes.find(hero => hero.name.toLowerCase() === name.toLowerCase())
  }

  delete(idParam: number): void {
    this.heroes = this.heroes.filter(hero => hero.id !== idParam)
  }
}
