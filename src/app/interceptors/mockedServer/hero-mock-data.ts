import {Hero} from "../../models/hero";

let firstId = 1;
export const idGenerator = () => {
  return firstId++;
}

export const factoryMockData = (multiplyFactor: number): Hero[] => {
  let mock: Hero[] = heroMockData();

  for (let i = 0; i < multiplyFactor - 1; i++) {
    mock = [...mock, ...heroMockData()]
  }

  return mock;
}

export let heroMockData = (): Hero[] => ([
  {
    "id": idGenerator(),
    "name": "Spider-Man",
    "realName": {
      "firstName": "Peter",
      "middleName": "Benjamin",
      "lastName": "Parker"
    },
  },
  {
    "id": idGenerator(),
    "name": "Iron Man",
    "realName": {
      "firstName": "Tony",
      "middleName": "",
      "lastName": "Stark"
    },
  },
  {
    "id": idGenerator(),
    "name": "Wonder Woman",
    "realName": {
      "firstName": "Diana",
      "middleName": "",
      "lastName": "Prince"
    },
  },
  {
    "id": idGenerator(),
    "name": "Black Widow",
    "realName": {
      "firstName": "Natasha",
      "middleName": "",
      "lastName": "Romanoff"
    },
  },
  {
    "id": idGenerator(),
    "name": "Captain America",
    "realName": {
      "firstName": "Steve",
      "middleName": "Grant",
      "lastName": "Rogers"
    },
  },
  {
    "id": idGenerator(),
    "name": "Thor",
    "realName": {
      "firstName": "Thor",
      "middleName": "",
      "lastName": "Odinson"
    },
  },
])
