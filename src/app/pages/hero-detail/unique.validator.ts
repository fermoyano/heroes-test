import {Injectable} from '@angular/core';
import {AbstractControl, AsyncValidator, AsyncValidatorFn, ValidationErrors} from "@angular/forms";
import {map, Observable} from 'rxjs';
import {HeroService} from "../../services/hero/hero.service";

@Injectable({
  providedIn: 'root'
})
export class UniqueValidator implements AsyncValidator {

  constructor(
    private heroService: HeroService
  ) {
  }

  validate(control: AbstractControl<string>, id?: number): Observable<ValidationErrors | null> {
    return this.heroService.checkHeroName(control.value, id).pipe(
      map(existing => {
        if (existing) {
          return {nameTaken: true}
        }
        return null;
      })
    )
  }

  createValidator(id?: number): AsyncValidatorFn {
    return (control: AbstractControl) => this.validate(control, id);
  }
}
