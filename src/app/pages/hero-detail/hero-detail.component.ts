import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {LayoutModule} from "../../layout/layout.module";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {map, Observable, of, switchMap, tap} from "rxjs";
import {HeroService} from "../../services/hero/hero.service";
import {Hero} from "../../models/hero";
import {DisplayMayusDirective} from "../../directives/display-mayus.directive";
import {UniqueValidator} from "./unique.validator";
import {MatError} from "@angular/material/form-field";

interface Form {
  name: FormControl<string>;
  realName: FormGroup<RealNameForm>;
}

interface RealNameForm {
  firstName: FormControl<string>;
  middleName: FormControl<string>;
  lastName: FormControl<string>;
}

interface HeroDetail {
  mode: string;
  entityData?: Hero;
}

@Component({
  selector: 'hr-hero-detail',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, LayoutModule, RouterLink, DisplayMayusDirective, MatError],
  templateUrl: './hero-detail.component.html',
  styleUrl: './hero-detail.component.scss'
})
export class HeroDetailComponent {

  form: FormGroup;
  model$: Observable<HeroDetail>;
  id: number;

  constructor(
    fb: FormBuilder,
    route: ActivatedRoute,
    private router: Router,
    private heroService: HeroService,
    private uniqueValidator: UniqueValidator
  ) {
    this.id = parseInt(route.snapshot.params['id']);
    this.form = this.buildForm(fb);
    this.model$ = this.buildModel(route);
  }

  buildForm(fb: FormBuilder): FormGroup<Form> {
    return fb.nonNullable.group({
      name: fb.nonNullable.control('', {
        validators: Validators.required,
        asyncValidators: this.uniqueValidator.createValidator(this.id),
      }),
      realName: fb.nonNullable.group({
        firstName: ['', Validators.required],
        middleName: [''],
        lastName: ['', Validators.required],
      }),
    });
  }

  buildModel(route: ActivatedRoute): Observable<HeroDetail> {
    return route.data.pipe(
      map(data => data['mode']),
      switchMap(mode => {
        if (mode !== 'new') {
          return this.heroService.getById(this.id).pipe(
            tap(data => this.setFormData(data, mode)),
            map(data => ({entityData: data, mode})),
          )
        }

        return of({mode})
      }),
    );
  }

  setFormData(data: Hero | undefined, mode: string): void {
    this.form.patchValue({...data}, {emitEvent: false});
    if (mode === 'view') {
      this.form.disable({emitEvent: false});
    }
  }

  save(id?: number): void {
    const hero = this.form.value
    if (!id) {
      this.heroService.add(hero).subscribe(() => {
        this.router.navigate(['/heroes']);
      });
    } else {
      this.heroService.edit({id, ...hero}).subscribe(() => {
        this.router.navigate(['/heroes']);
      });
    }
  }

  delete(hero: Hero) {
    const userRes = confirm(`Esta seguro que desea eliminar: ${hero.name}?`);

    if (!userRes) {
      return
    }

    this.heroService.delete(hero.id).subscribe(() => {
      this.router.navigate(['/heroes']);
    });
  }
}
