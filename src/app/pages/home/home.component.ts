import {Component} from '@angular/core';
import {LayoutModule} from "../../layout/layout.module";
import {HeroCardComponent} from "./hero-card/hero-card.component";
import {HeroService} from "../../services/hero/hero.service";
import {combineLatest, map, Observable, startWith} from "rxjs";
import {AsyncPipe, JsonPipe, NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import {MatRipple} from "@angular/material/core";
import {Hero} from "../../models/hero";
import {Page} from "../../models/paginated-request";
import {PaginatorData} from "../../layout/paginator/paginator.component";

@Component({
  selector: 'hr-home',
  standalone: true,
  imports: [LayoutModule, HeroCardComponent, AsyncPipe, RouterLink, MatRipple, NgIf, JsonPipe],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {

  heroes$: Observable<Hero[]>;
  page$: Observable<PaginatorData>;

  constructor(
    private heroService: HeroService
  ) {
    this.page$ = combineLatest([
      this.heroService.pageParams$,
      this.heroService.itemCount$,
    ]).pipe(
      map(([page, length]) => ({...page, length})),
      startWith({
        pageIndex: 0,
        pageSize: 10,
        length: 0
      })
    )

    this.heroes$ = combineLatest([
      this.heroService.getAll(),
      this.page$,
    ]).pipe(
      map(([heroes, page]) => {
        const {pageIndex, pageSize} = page;
        return heroes.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize)
      })
    )
  }

  filter(searchTerm: string) {
    this.heroService.filter({name: searchTerm})
  }

  pageChange(page: Page) {
    this.heroService.setPage(page);
  }
}
