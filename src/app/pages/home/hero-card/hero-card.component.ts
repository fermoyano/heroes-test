import {Component, Input} from '@angular/core';
import {LayoutModule} from "../../../layout/layout.module";
import {NamePipe} from "../../../pipes/name.pipe";
import {Hero} from "../../../models/hero";
import {NgIf} from "@angular/common";

@Component({
  selector: 'hr-hero-card',
  standalone: true,
  imports: [
    LayoutModule,
    NamePipe,
    NgIf,
  ],
  templateUrl: './hero-card.component.html',
  styleUrl: './hero-card.component.scss'
})
export class HeroCardComponent {

  @Input() hero!: Hero;

}
