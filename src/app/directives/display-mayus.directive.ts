import {Directive, HostBinding} from '@angular/core';

@Directive({
  selector: '[hrDisplayMayus]',
  standalone: true
})
export class DisplayMayusDirective {

  @HostBinding('style.text-transform') get textTransform() {
    return 'uppercase'
  };
}
