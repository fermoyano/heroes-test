import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoadingRequestService {

  private isLoading$ = new BehaviorSubject<boolean>(false)

  setLoading(isLoading: boolean): void {
    this.isLoading$.next(isLoading);
  }

  get loadingStatus$(): Observable<boolean> {
    return this.isLoading$;
  }

}
