import {TestBed} from '@angular/core/testing';
import {HeroFilters, HeroService} from './hero.service';
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {heroMockData} from "../../interceptors/mockedServer/hero-mock-data";
import {provideHttpClient} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {provideRouter} from "@angular/router";

describe('HeroService', () => {
  let service: HeroService;
  let httpClientTesting: HttpTestingController;
  let mockData = heroMockData();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HeroService,
        provideHttpClient(),
        provideHttpClientTesting(),
        provideRouter([])
      ],
    });

    service = TestBed.inject(HeroService);
    httpClientTesting = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should be able to get the heroes', async () => {
    const heroes$ = service.getAll();
    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.method).toBe('GET')

    req.flush(mockData);
    expect(await response).toBe(mockData);
  })

  it('Should set the default pagination headers to request', async () => {
    const heroes$ = service.getAll();
    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.headers.get('page-index')).toBe('0');
    expect(req.request.headers.get('page-size')).toBe('10');

    req.flush(mockData);
    expect(await response).toBe(mockData);
  })

  it('Should set the filter to request as queryParams', async () => {
    const filters: HeroFilters = {
      name: 'mock-name'
    }
    const heroes$ = service.getAll();
    service.filter(filters);

    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.params.get('name')).toBe('mock-name');

    req.flush(mockData);
    expect(await response).toBe(mockData);
  })

  it('Should be able get a hero', async () => {
    const hero = mockData[0]
    const heroes$ = service.getById(hero.id);
    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.method).toBe('GET')

    req.flush(hero);

    expect(await response).toBe(hero);
  })

  it('Should be able to edit the heroes', async () => {
    const editedHero = {...mockData[0], name: 'updated name'}
    const heroes$ = service.edit(editedHero);
    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.method).toBe('PUT')

    req.flush(editedHero);

    expect(await response).toBe(editedHero);
  })

  it('Should be able to delete the heroes', async () => {
    const heroes$ = service.delete(0);
    const response = firstValueFrom(heroes$);

    const req = httpClientTesting.expectOne(
      request => request.url.includes('/heroes'),
    );

    expect(req.request.method).toBe('DELETE')
    req.flush(null);
    expect(await response).toBeNull();
  })

  afterEach(() => {
    httpClientTesting.verify();
  });
});
