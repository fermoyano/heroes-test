import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, finalize, map, Observable, switchMap, tap} from "rxjs";
import {Hero} from "../../models/hero";
import {PaginatedRequest} from "../../models/paginated-request";

export interface HeroFilters {
  name: string;
}

@Injectable({providedIn: "root"})
export class HeroService extends PaginatedRequest<Hero> implements OnDestroy {

  url = 'http://localhost:8100/heroes';
  initialFilters: HeroFilters = {
    name: '',
  };

  filters$ = new BehaviorSubject<HeroFilters>(this.initialFilters);

  override getAll(): Observable<Hero[]> {
    return this.filters$.pipe(
      tap(filters => this.setParams(filters)),
      switchMap(() => super.getAll()),
    );
  }

  filter(filters: HeroFilters) {
    this.filters$.next(filters);
  }

  getById(id: number): Observable<Hero | undefined> {
    return this.http.get<Hero>(`${this.url}/${id}`);
  }

  edit(hero: Hero): Observable<Hero> {
    return this.http.put<Hero>(`${this.url}/${hero.id}`, hero)
      .pipe(finalize(() => this.updateData()));
  }

  add(hero: Omit<Hero, 'id'>): Observable<Hero> {
    return this.http.post<Hero>(`${this.url}`, hero)
      .pipe(finalize(() => this.updateData()));
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}/${id}`)
      .pipe(finalize(() => this.updateData()));
  }

  checkHeroName(name: string, id?: number) {
    return this.http.get<Hero>(`${this.url}`, {params: {name}}).pipe(
      map(hero => hero && id ? hero.id !== id : !!hero),
    )
  }

  ngOnDestroy() {
    this.destroy$.next(true)
  }

}

