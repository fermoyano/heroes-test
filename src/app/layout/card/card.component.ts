import {Component} from "@angular/core";

@Component({
  selector: 'hr-card',
  template: `
    <mat-card>
      <mat-card-content>
        <ng-content></ng-content>
      </mat-card-content>
    </mat-card>
  `,
})
export class CardComponent {
}
