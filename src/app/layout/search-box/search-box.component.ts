import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, Subject, takeUntil} from 'rxjs';

@Component({
  selector: 'hr-search-box',
  templateUrl: './search-box.component.html',
  styleUrl: './search-box.component.scss'
})
export class SearchBoxComponent implements OnDestroy {

  @Output() search = new EventEmitter<string>();

  control = new FormControl('');
  destroyObservable$ = new Subject();

  constructor() {
    this.control.valueChanges.pipe(
      debounceTime(200),
      takeUntil(this.destroyObservable$)
    ).subscribe(value => {
      if (value) {
        this.search.emit(value)
        return;
      }
      this.search.emit('')
    });
  }

  ngOnDestroy(): void {
    this.destroyObservable$.next(true)
  }
}
