import {ComponentFixture, TestBed} from '@angular/core/testing';
import {InputComponent} from './input.component';
import {LayoutModule} from "../layout.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('InputComponent', () => {
  let component: InputComponent;
  let fixture: ComponentFixture<InputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LayoutModule, BrowserAnimationsModule],
    })
      .compileComponents();

    fixture = TestBed.createComponent(InputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
