import {Component, forwardRef, Input, OnDestroy} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator,
  ValidatorFn,
} from "@angular/forms";
import {Subject, takeUntil} from "rxjs";
import {ErrorStateMatcher} from "@angular/material/core";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  constructor(private control: AbstractControl | null) {
  }

  isErrorState(): boolean {
    console.log(this.control)
    return this.control?.invalid || false;
  }
}

@Component({
  selector: 'hr-input',
  templateUrl: './input.component.html',
  styleUrl: './input.component.scss',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }, {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    }
  ]
})
export class InputComponent implements ControlValueAccessor, OnDestroy, Validator {

  @Input() placeholder = '';
  @Input() label = '';

  control = new FormControl<string>('');
  destroy$ = new Subject();

  onChange = (_: string) => {
  }
  onTrouched = () => {
  }

  constructor() {
    this.control.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(value => this.onChange(value || ''));
  }

  validate(control: AbstractControl): ValidatorFn | null {
    const validators = control.validator;
    const asyncValidators = control.asyncValidator;
    this.control.setValidators(validators)
    this.control.setAsyncValidators(asyncValidators)

    return null;
  }

  writeValue(value: string): void {
    this.control.setValue(value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTrouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.control.disable({emitEvent: false});
      return
    }
    this.control.enable({emitEvent: false});
  }

  ngOnDestroy() {
    this.destroy$.next(true);
  }

}
