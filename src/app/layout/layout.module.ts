import {NgModule} from "@angular/core";
import {CardComponent} from "./card/card.component";
import {MatCardModule} from '@angular/material/card';
import {HeaderComponent} from "./header/header.component";
import {InputComponent} from "./input/input.component";
import {SearchBoxComponent} from "./search-box/search-box.component";
import {MatError, MatInputModule} from "@angular/material/input";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {PageLayoutComponent} from "./page-layout/page-layout.component";
import {ButtonComponent} from "./button/button.component";
import {MatButton} from "@angular/material/button";
import {MatToolbar} from "@angular/material/toolbar";
import {MatProgressBar} from "@angular/material/progress-bar";
import {LoadingIndicatorComponent} from "./loading-indicator/loading-indicator.component";
import {PaginatorComponent} from "./paginator/paginator.component";
import {MatPaginator} from "@angular/material/paginator";

const components = [
  CardComponent,
  HeaderComponent,
  InputComponent,
  SearchBoxComponent,
  PageLayoutComponent,
  ButtonComponent,
  LoadingIndicatorComponent,
  PaginatorComponent,
]

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatButton,
    MatToolbar,
    MatProgressBar,
    MatPaginator,
    MatError
  ],
  exports: [...components],
  declarations: [...components],
})
export class LayoutModule {
}
