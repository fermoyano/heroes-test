import {Component, Input} from '@angular/core';

type Colors = 'primary' | 'accent' | 'warn';

@Component({
  selector: 'hr-button',
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {

  @Input() color: Colors = "primary";
  @Input() disabled = false;

}
