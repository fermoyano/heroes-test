import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {LoadingRequestService} from "../../services/loading-request/loading-request.service";

@Component({
  selector: 'hr-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrl: './loading-indicator.component.scss'
})
export class LoadingIndicatorComponent {

  isLoading$: Observable<boolean>;

  constructor(
    loadingService: LoadingRequestService
  ) {
    this.isLoading$ = loadingService.loadingStatus$;
  }
}
