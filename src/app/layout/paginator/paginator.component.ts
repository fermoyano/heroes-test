import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PageEvent} from "@angular/material/paginator";

export interface PaginatorData {
  pageIndex: number;
  pageSize: number;
  length: number;
}

@Component({
  selector: 'hr-paginator',
  templateUrl: './paginator.component.html',
})
export class PaginatorComponent {

  @Input() page: PaginatorData = {
    pageIndex: 1,
    pageSize: 10,
    length: 0,
  };
  @Output() pageChange = new EventEmitter<Omit<PaginatorData, 'length'>>();

  pageSizeOptions = [5, 10, 25, 100];

  emitPageChange(event: PageEvent) {
    this.pageChange.emit({
      pageSize: event.pageSize,
      pageIndex: event.pageIndex
    });
  }

}
