import {Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {HeroDetailComponent} from "./pages/hero-detail/hero-detail.component";


export const routes: Routes = [
  {path: '', redirectTo: '/heroes', pathMatch: 'full'},
  {
    path: 'heroes', children: [
      {path: '', component: HomeComponent},
      {
        path: 'edit/:id', component: HeroDetailComponent, data: {
          mode: 'edit'
        }
      },
      {
        path: 'view/:id', component: HeroDetailComponent, data: {
          mode: 'view'
        }
      },
      {
        path: 'new', component: HeroDetailComponent, data: {
          mode: 'new'
        }
      },
    ]
  },
];
