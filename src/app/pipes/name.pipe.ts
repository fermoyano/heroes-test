import {Pipe, PipeTransform} from '@angular/core';
import {Name} from "../models/hero";

@Pipe({
  name: 'name',
  standalone: true
})
export class NamePipe implements PipeTransform {

  transform(name: Name): string {
    return `${name.lastName}, ${name.firstName} ${name.middleName}`;
  }

}
