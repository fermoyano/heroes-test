# Heroes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7.

## RUN

El proyecto puede levantarse de dos formas:

#### npm
  ```
    $ npm install
    $ npm start
  ```
#### docker-compose

 ```
  $ docker-compose build
  $ docker-compose up
```

## Consideraciones

- Dentro de 'loading-interceptor' se encuentra un delay de unos segundos ya que al no conectarse a un servidor real, no se mostraría nunca el loading en la barra superior
- Se pueden encontrar sintaxis de template mezclada entre la clásica y la nueva de angular 17 (@if por ejemplo), esto es con fines demostrativos no un error.
- Si el modelo de datos es bastante simple, en caso de querer complejizarlo no tendría problema
- se utilizó un interceptor para bloquear las request hacia backend y responder con un modelo de datos especifico, en un proyecto real no llevaría a cabo esta práctica, pero permite que el código se ejecute son servidor de una forma bastante real.
- Dentro de la directiva para transformar a mayúsculas, consideré que era meramente demostrativo, por ello utilice css para transformar el texto
- No utilice la tabla provista por material para mostrar los datos, solo con el fin de poder escribir y mostrar un poco más de CSS.
- El proyecto tiene una pequeña configuración de Ci que corre los test al pushear y buildea la app.
